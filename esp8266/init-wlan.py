# This file is executed on every boot (including wake-boot from deepsleep)
#import esp
#esp.osdebug(None)
import uos, machine
#uos.dupterm(None, 1) 
# disable REPL on UART(0)
import gc
#import webrepl
#webrepl.start()
gc.collect()
import network
if not('wlan-key.txt' in uos.listdir()):
     exec(open('write-wifi-credentials.py').read(),globals())
sta_if = network.WLAN(network.STA_IF)
sta_if.active(True)
f=open('wlan-key.txt')
s=f.readline()
SSID=s.split('\n')[0]
s=f.readline()
WPAkey=s.split('\n')[0]
f.close()
sta_if.connect(SSID,WPAkey)
import time
tries=0
while (sta_if.isconnected()==False):
    time.sleep(1) # wait for network
    tries+=1
    if tries > 3:
        break
    sta_if.ifconfig()
    # set time
    import ntptime
    ntptime.settime()
    time.localtime()
