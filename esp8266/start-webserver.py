try:
  import usocket as socket
except:
  import socket
import network
import esp
esp.osdebug(None)
import gc
import urequests
import uselect as select
import json
gc.collect()

# container for list of devices
devices=list()
# switch for auto Setting
auto = False

# returns IP adresses, names, and on/off times of devices 
def get_list_of_devices():
    # get list of devices from file
    devices=list()
    f=open('devicelist.txt')
    while True:
    	line = f.readline().strip()
	if line == '':
    	    break
	devices.append(json.loads(line))
    f.close()
    return devices
    #numdevices=len(devices)

#def page_device_list():
#    get_list_of_devices()

#def set_list_of_devices():

# sets state of device
#def set_state(device,val)

# gets current state of device
def get_state():
    get_list_of_devices()
    state=list()
    for device in devices:
        response=urequests.get("http://"+device["ip"]+"/cm?cmnd=State")
        state.append(response.json()["POWER"])
    return state

def page_toggle_devices():
  state=get_state()
  html = """<html><head> <title>ESP Web Server</title> <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="data:,"> <style>html{font-family: Helvetica; display:inline-block; margin: 0px auto; text-align: center;}
  h1{color: #0F3376; padding: 2vh;}p{font-size: 1.5rem;}.button{display: inline-block; background-color: #e7bd3b; border: none; 
  border-radius: 4px; color: white; padding: 16px 40px; text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}
  .button2{background-color: #4286f4;}</style></head><body> <h1>ESP Web Server</h1> 
  <p>GPIO state: <strong>""" + gpio_state + """</strong></p><p><a href="/?led=on"><button class="button">ON</button></a></p>
  <p><a href="/?led=off"><button class="button button2">OFF</button></a></p></body></html>"""
  return html

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 80))
s.listen(5)

def Client_handler(conn):
  request = conn.recv(1024)
  request = str(request)
  print('Content = %s' % request)
  #led_on = request.find('/?led=on')
  response = web_page()
  conn.send('HTTP/1.1 200 OK\n')
  conn.send('Content-Type: text/html\n')
  conn.send('Connection: close\n\n')
  conn.sendall("Test "+str(i))
  conn.close()

i=0
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 80))
s.listen(5)

while True:
    r, w, err = select.select((s,), (), (), 1)
    if r:
        for readable in r:
            conn, addr = s.accept()
            try:
                Client_handler(conn)
            except OSError as e:
                pass
    curr_time=time.localtime()
    curr_slice=(curr_time[3]*60+curr_time[4]) // 15
    if curr_slice!=last_slice:
        # get_state
        if record:
            state=get_state()
            i=0
            for device in devices:
                if state[i]=="ON":
                    device["auto"][curr_slice]=1
                else:
                    device["auto"][curr_slice]=0
                i=i+1
            savedevices(devices)
        # set_state
        if auto:
            for device in devices:
                toggle_device(device["ip"],device["auto"][curr_slice])
    last_slice=curr_slice
