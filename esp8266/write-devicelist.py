#write devicelist
import json
f=open('devicelist.txt','w')
#Ask for Devices
next=True
while next:
	device=dict({}) 
	device["name"]=input('Enter Device-Name (leave empty to finish):')
	if device["name"]!="" :
		device["ip"]=input('Enter Device-IP:')
		device["auto"]=[0]*24*4
		f.write(json.dumps(device)+'\n')
	else:
		next=False
f.close()
